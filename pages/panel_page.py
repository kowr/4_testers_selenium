from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class PanelPage:
    def __init__(self, browser):
        self.browser = browser

    def wait_for_load(self):
        wait = WebDriverWait(self.browser, 10)
        response_button_selector = (By.CSS_SELECTOR, '.j_msgResponse')
        return wait.until(EC.element_to_be_clickable(response_button_selector))

    def send_message(self, message_text):
        self.browser.find_element(By.CSS_SELECTOR, '#j_msgContent').send_keys(message_text)
        self.browser.find_element(By.CSS_SELECTOR, '.j_msgResponse').click()
