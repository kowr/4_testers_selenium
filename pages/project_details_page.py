from selenium.webdriver.common.by import By


class ProjectDetailsPage:
    def __init__(self, browser):
        self.browser = browser

    def close_new_project_confirmation(self):
        self.browser.find_element(By.CSS_SELECTOR, '.j_close_button').click()

    def click_cockpit(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin').click()
