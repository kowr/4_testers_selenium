from selenium.webdriver.common.by import By


class CockpitPage:
    def __init__(self, browser):
        self.browser = browser

    def click_envelope(self):
        self.browser.find_element(By.CSS_SELECTOR, '.top_messages').click()

    def click_administration(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin').click()
