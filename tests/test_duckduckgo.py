from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import time


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://duckduckgo.com/')

    # Znalezienie paska wyszukiwania
    search_box_input = browser.find_element(By.CSS_SELECTOR, '#search_form_input_homepage')

    # Znalezienie guzika wyszukiwania (lupki)
    search_button_input = browser.find_element(By.CSS_SELECTOR, '#search_button_homepage')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_box_input.is_displayed() is True
    assert search_button_input.is_displayed() is True

    # Szukanie Vistula University
    search_box_input.send_keys('Vistula University')
    search_button_input.click()

    # Sprawdzenie że pierwszy wynik ma tytuł 'Vistula University in Warsaw'
    search_results = browser.find_elements(By.CSS_SELECTOR, '.nrn-react-div')
    assert len(search_results) > 2

    # Sprawdzenie że pierwszy wynik ma tytuł 'Vistula University in Warsaw'
    first_result = browser.find_element(By.CSS_SELECTOR, '#r1-0 h2 span')
    first_result_test = first_result.text
    assert first_result_test == 'Home - Vistula University'

    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()


